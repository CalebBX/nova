###########Changelog##############

01/03/2017 - 1.0.0

##ADDED
-Fighter ship prefab for player
-basic player movement
-game background
-mobile controls
	-joystick
	-fire button




01/17/2017 1.0.1

##ADDED
-Afterburner particle system for player ship
-Enemy Ship
	-Enemy Prefab
	-simple tracking enemy AI




01/25/2017 1.0.2

##ADDED
-Camera tracker

02/01/2017 1.0.2b
##ADDED
-particle star system
##CHANGED
-Camera tracker to allow for zooming in and out based on targets.
-Game background to add a paralax scrolling effect




02/05/2017 1.0.3

##ADDED 
-targeting system for camera tracker
##FIXED 
-android build functionality
##REMOVED 
-particle star system for performance reasons




02/10/2017 1.0.4

##ADDED
-bullet prefab
-fire method for player
-bullet collision methods
-Enemy Health




02/17/2017 1.0.4b

##ADDED 
-bullet explosion particle system
-enemy explosion particle system
-die method for enemy
##CHANGED
-bullet collision script




2/20/2017 1.0.5

##ADDED
-Cruiser ship prefab for player
##CHANGED 
-Ship system to separate player and ship to all for modularized and expandable ship selection.
-Fighter prefab sprite
-shipController script to allow for multiple afterburners
-Afterburner particle system graphics, material, color, and rate




2/23/2017 1.0.6

##ADDED
-turret prefab
-turretController script
	-allows for tracking using global targeting for nearest enemy to ship
##CHANGED
-targeting system to accomidate turrets
-existing fire/gun system
	-modularized gun system to separate cannons and turrets




2/25/2017 1.0.6b

##CHANGED 
-turretController script
	-Turrets now use a local targeting algorithm
	-Turrets now pick the closed enemy to the individual turret




2/28/2017 1.0.6c

##ADDED
-Laser turret prefab
-Laser turret controller
	-Uses similar algorithm to main turret, but used raycasting and a line renderer




3/01/2017 1.0.6d

##CHANGED
-turret controller loop procedure
	-all guns and turrets use the same type of firing loop
	-allows for modifiable fire rates




3/03/2017 1.0.7

##ADDED
-enemy fire procedure
-colliders to enemy prefab to prevent flocking
##CHANGED
-enemy AI to Accommodate attacking




3/04/2017

##ADDED 1.0.7
-astroid sprites
-astroid prefab and colliders
-raycasting selection procedure for manual selection
	-currently unfunctional



3/10/2017 1.0.8
##ADDED
-Random astroid map spawner
##CHANGED
-astroids, now collision functional



3/13/2017 1.0.9b
##ADDED
-Random astroid group spawner



3/15/2017 1.0.9c
##CHANGED
-Astroid group spawner spawns all types of astroids
	-now works will failcase to reduce load times and controll astroid density.
	-now astroid spawn amount and raidius can be publicly controlled



3/29/2017 1.0.10
##ADDED
-Functional starfield system using seamless textures instead of particle systems
-Planets(sprites not owned)
-Paralax scroll system for planets



4/8/2017 1.0.11
##ADDED
-map size computer to calculate virtual size of map based on paralax scroll speed (took a lot of time)
-editor functions to display virtual map size



4/10/2017 1.0.12
##ADDED
-clamp function based on map size to keep player inbounds
##CHANGED 
-Astroid map spawner now uses virtual map size



4/20/2017 1.0.13
##ADDED
-shop UI and switchable ships (placeholder)



4/25/2017 1.0.14
##CHANGED
-refined all ui text and controll elements to make more readable and visually appealing
-refined shop UI



4/28/2017 1.0.15
##CHANGED
-resized ui and controls for smaller phones
##FIXED
-all android build issues resolved



5/1/2017 1.1.0
--------------------------------THE PARALAX UPDATE------------------------------------------
##ADDED
-new paralax system using a perspective 3d camera and a z coordinate depth
	-will be much easier to implement for future developments such as random map generation and minimaps
	-significantly reduced cpu strain (scroller no longer moving multiple high resolution sprites in unison with player)
##REMOVED
-current paralax scroll system removed
-all of the map size calculation algorithms removed (apparently huge waste of time)
-everything related to the old orthographic camera removed
-player boundry clamp function removed
-planet scrollers removed
##KNOWN BUGS
-starfield now broken
-enemy death now broken



5/3/2017 1.1.1
##FIXED
-starfield now works again
-enemy death works again
##CHANGED
-optimized camera rig and simplified to remove old targeting alorithms no longer in use
-began work on player death UI



5/4/2017 1.1.2
##ADDED
-player death
-player death screen
-fade transition for death screen
-scene restart
-increased base map size
##CHANGED
-locked screen orientation for mobile devices
##FIXED
-android build functionality



5/7/2017 1.1.3
##ADDED
-Random planet spawning



5/14/2017 1.2.0
----------------------------------------THE WEAPON UPDATE---------------------------------
##ADDED
-Hardpoint system
##CHANGED
-Cannon controller now works with hardpoint system
-Laser controller now works with hardpoint system



5/14/2017 1.2.0b
##CHANGED
-Enemies now also utilize hardpoint system
-turrets utilize hardpoint system



5/15/2017 1.2.0c
##CHANGED
-modified ship prefabs to use hardpoint system



5/17/2017 1.2.1
##ADDED
-Damage modifier to bullets
-Unity collab support



5/18/2017 1.2.2
##ADDED
-Black Hole/WarpGate



5/19/2017 1.2.3 (UNSTABLE)
##CHANGED
-Updated persistant data storing methods
-Started separating enemy AI from enemy controller
	-This will allow multiple enemy types other than just fighters



5/21/2017 1.2.4
##CHANGED
-Completed modularizing enemies and separating AI from Controllers



5/23/2017 1.3.0
--------------------------------------THE MAP UPDATE------------------------------------
##ADDED
-enemy base prefab
	-enemy base controller
	-enemy turret controller
	-enemy base sprite
-Random spawning for planets
-Random astroid group spawning
-random enemy spawning
-random enemy base spawning
-random map background spawning
-new map sprites
##FIXED
-broken lasers fixed
##KNOWN BUGS
-Turrets broken
-When enemy base is destroyed enemies are destroyed as well



5/24/2017 1.3.1
##ADDED
-Enemy random group spawning
##CHANGED
separated enemy group spawner are enemy base
##FIXED
-Enemy bases no longer destroy children



5/25/2017 1.4.0
------------------------------------THE MISSILE UPDATE------------------------------------
##ADDED 
-missile prefab
-missile thrusters
-missile launcher
-Guided missile system using colliders
##CHANGED
-target system accomodates missiles
-layer system accomoodates missiles
##KNOWN BUGS
-Cannon bullet sprites now broken



5/26/2017 1.4.1
##FIXED
-enemy base bullets now are displaying properly
-enemy cannon bullets now display properly



5/28/2017 1.5.0
--------------------------------THE INVENTORY UPDATE-----------------------------------------
##ADDED
-New inventory system
-Inventory UI
-Items class
-Item class
-Items text file holding database of all available items




5/29/2017 1.5.1
##ADDED
-Scene inventory item
-Fully functional fitting and inventory system
##CHANGED
-overhauled the way inventory items were stored and added
-overhauled inventory UI
-changed to fitting ui
-inventory ui now fully functional
-inventory ui now works with hardpoint system




6/1/2017 1.6.0
----------------------------------THE PC UPDATE-----------------------------------------------
##CHANGED
-build type to pc
-controls to match pc controls
-field of view to accomodate larger screen
-physics movment system using torque to rotate ships
##REMOVED
-all mobile controlls
-crossplatform input classes and methods




6/2/2017 1.6.1
##ADDED
-static data controller
	-holds
		-money
		-inventory
		-xp
		-gamestate
##CHANGED
-items list now static class
	-can be accessed and pulled from anywhere without an instance
-Overhauled loadout system
-changed both hardpoint and inventory systems to work in unison


6/3/2017 1.6.2
##ADDED
-HUD offscreen enemy tracker



6/4/2017 1.6.2b
##CHANGED
-offscreen tracker now works for any gameobject
-separated tracker and indicator classes
##FIXED
-Fighters now unbroken (layer mask issue)



6/5/2017 1.7.0
-------------------------------------Tutorial Update------------------------------------------
##ADDED
-messagebox class
-retro style text display
-tutorial scene



6/6/2017 1.7.1
##ADDED
-Worldspace tutorial ui messages
-inactive enemies to tutorial scene
-fitting ui messagebox




6/7/2017 1.7.2
##ADDED
-game reseter
##FIXED
-player hardpoint conflict with enemies
-fixed broken death system

----------------------------------------------------------------------------
final optimizations and packaging out for presentation