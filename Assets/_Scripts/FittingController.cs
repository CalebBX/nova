﻿
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class FittingController : MonoBehaviour
{
    public GameObject inventorySlot;
    public Transform inventoryGroup;
    public GameObject hardpointSlot;
    public Transform hardpointGroup;
    private Canvas mainCanvas;
    private ShipController ship;
    private InventorySlotController[] inventorySlots;
    private HardpointSlotController[] hardpointSlots;
    private int selectedHardpointSlot;
    private Sprite defaultSprite;

    private void Start()
    {
        mainCanvas = GetComponent<Canvas>();
        inventorySlots = inventoryGroup.gameObject.GetComponentsInChildren<InventorySlotController>();
        for(int i = 0; i < inventorySlots.Length; i++)
        {
            inventorySlots[i].setSlotNum(i);
        }
        ship = GameObject.FindGameObjectWithTag("Ship").GetComponent<ShipController>();
        defaultSprite = inventorySlots[0].GetComponent<Image>().sprite;

        shipChange(ship);
        startingItems();
        updateItems();
        updateLoadout();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (mainCanvas.enabled)
            {
                mainCanvas.enabled = false;
            }
            else
            {
                mainCanvas.enabled = true;
                updateItems();
            }
        }
    }
    private void updateItems()
    {
        for (int i = 0; i < DataController.inventory.Length; i++)
        {
            if(DataController.inventory[i] != null)
            {
                inventorySlots[i].item = DataController.inventory[i];
                inventorySlots[i].hasItem = true;
                inventorySlots[i].gameObject.GetComponent<Image>().sprite = inventorySlots[i].item.getSprite();
            }
            else
            {
                inventorySlots[i].hasItem = false;
                inventorySlots[i].gameObject.GetComponent<Image>().sprite = defaultSprite;
            }

        }
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            if (hardpointSlots[i].hasItem)
            {
                hardpointSlots[i].gameObject.GetComponent<Image>().sprite = hardpointSlots[i].item.getSprite();
            }
        }
    }

    public void selectHardpoint(int slotNum)
    {
        selectedHardpointSlot = slotNum;
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            hardpointSlots[i].GetComponent<Button>().interactable = true;
        }
        hardpointSlots[slotNum].GetComponent<Button>().interactable = false;
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            if (inventorySlots[i].hasItem)
            {
                inventorySlots[i].GetComponent<Button>().interactable = true;
            }
        }
    }
    public void selectItem(int slotNum)
    {
        if (hardpointSlots[selectedHardpointSlot].hasItem)
        {
            Item temp = hardpointSlots[selectedHardpointSlot].item;
            hardpointSlots[selectedHardpointSlot].item = DataController.inventory[slotNum];
            DataController.setItem(slotNum,temp);
        }
        else
        {
            hardpointSlots[selectedHardpointSlot].item = inventorySlots[slotNum].item;
            hardpointSlots[selectedHardpointSlot].hasItem = true;
            DataController.removeItem(slotNum);
        }
        hardpointSlots[selectedHardpointSlot].GetComponent<Button>().interactable = true;
        updateItems();
        updateLoadout();
    }
    public void updateLoadout()
    {
        for (int i = 0; i < ship.hardpoints.Length; i++)
        {
            foreach (Transform child in ship.hardpoints[i].transform)
            {
                Destroy(child.gameObject);
            }
        }
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            Transform hardpointTransform = ship.hardpoints[i].transform;
            if (hardpointSlots[i].hasItem)
            {
                Item currentItem = hardpointSlots[i].item;
                WeaponController weapon;
                weapon = Instantiate(currentItem.getGameObject(), hardpointTransform.position, hardpointTransform.rotation, hardpointTransform).GetComponent<WeaponController>();
                weapon.setStats(currentItem);
            }
        }
    }
    public void shipChange(ShipController newShip)
    {
        //Adds current mods to inventory
        if (hardpointSlots != null)
        {
            for (int i = 0; i < hardpointSlots.Length; i++)
            {
                if (hardpointSlots[i].hasItem)
                {
                    //addItem(hardpointSlots[i].item);
                }
                Destroy(hardpointSlots[i].gameObject);
            }
        }

        ship = newShip;
        hardpointSlots = new HardpointSlotController[ship.hardpoints.Length];
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            hardpointSlots[i] = Instantiate(hardpointSlot, hardpointGroup).GetComponent<HardpointSlotController>();
            hardpointSlots[i].setSlotNum(i);
        }

    }
    private void startingItems()
    {
        DataController.addItem(Items.getRandomItem());
    }
}
