﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turretController : MonoBehaviour
{
    public float fireRate;
    public Transform target;
    public Transform shotSpawn;
    public GameObject shot;
    public PlayerController playerScript;
    public float turnSpeed;
    private float nextFire;
    private List<Transform> targets;
    public ParticleSystem muzzleFlash;
    private HardpointController hardpoint;
    private bool fire;
    private float timer;
    public float damage;
    public WeaponController weapon;

    public bool UsesParentTarget;
    void Start()
    {
        playerScript = gameObject.GetComponentInParent<PlayerController>();
        hardpoint = GetComponentInParent<HardpointController>();
        weapon = gameObject.GetComponent<WeaponController>();
        loadStats();
    }
    private void loadStats()
    {
        damage = weapon.damage;
        fireRate = weapon.fireRate;
    }
    void Update()
    {
        fire = hardpoint.getFire();
        if (UsesParentTarget)
        {
            target = playerScript.curTarget;
        }
        else
        {
            targets = playerScript.targets;
            target = GetClosestTarget(targets);
        }
        
        if (target != null)
        {
            Vector3 dir = target.position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), turnSpeed);

            if (fire && Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
            }
            if (fire)
            {
                timer += Time.deltaTime;
                if (timer >= fireRate)
                {
                    shoot();
                }
            }

        }

    }
    Transform GetClosestTarget(List<Transform> targets)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in targets)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }
    private void shoot()
    {
        BulletMover bullet = Instantiate(shot, shotSpawn.position, shotSpawn.rotation).GetComponent<BulletMover>();
        bullet.setDamage(damage);
        muzzleFlash.Play();
        timer = 0;
    }
}

