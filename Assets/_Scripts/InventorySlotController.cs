﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlotController : MonoBehaviour {
    private int slotNum;
    public Item item;
    public bool hasItem = false;
    private FittingController2 fittingController;
    private void Start()
    {
        fittingController = FindObjectOfType<FittingController2>();
    }
    public void selectSlot()
    {
        if(hasItem)
        {
            fittingController.selectItem(slotNum);
        }

    }
    public void setSlotNum(int slotNum)
    {
        this.slotNum = slotNum;
    }
}
