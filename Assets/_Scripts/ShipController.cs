﻿using UnityEngine;


public class ShipController : MonoBehaviour
{

    public string shipName;
    public int shipPrice;
   
    public float speed;
    public float turnspeed;
    public ParticleSystem[] thrusters;
    public HardpointController[] hardpoints;
    public GameObject deathExplosion;
    public float sizeModifier;
    public float armor;
    bool isDead;

    private GameObject player;
    private Rigidbody2D rb;
    private PlayerController playerController;

    private void Awake()
    {
        hardpoints = gameObject.GetComponentsInChildren<HardpointController>();
    }
    void Start()
    {
        isDead = false;
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<PlayerController>();
        rb = player.GetComponent<Rigidbody2D>();
        //zoomToShipSize();
    }
    void Update()
    {
        
        if (isDead == false && armor <= 0)
        {
            die();
        }
    }
    void FixedUpdate()
    {
        
        if (Input.GetKey(KeyCode.Space))
        {
            foreach (HardpointController hardpoint in hardpoints)
            {
                hardpoint.setFire(true);
            }
        }
        else
        {
            foreach (HardpointController hardpoint in hardpoints)
            {
                hardpoint.setFire(false);
            }
        }
        if (Input.GetKey((KeyCode.UpArrow)))
        {
            rb.AddRelativeForce(new Vector3(0, 1, 0) * speed * Time.deltaTime);
            startThrusters();
        }
        else
        {
            stopThrusters();
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.AddRelativeForce(new Vector3(0, -1, 0) * (speed/2) * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.AddTorque(Time.deltaTime * turnspeed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddTorque(Time.deltaTime * -turnspeed);
        }
    }
    private void startThrusters()
    {
        if (thrusters[0].isStopped)
        {
            for (int i = 0; i < thrusters.Length; i++)
            {
                thrusters[i].Play();
            }
        }
    }
    private void stopThrusters()
    {
        if (thrusters[0].isPlaying)
        {
            for (int i = 0; i < thrusters.Length; i++)
            {
                thrusters[i].Stop();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Enemy_Shot")
        {
            armor -= coll.GetComponent<BulletMover>().getDamage();
        }

    }
    private void die()
    {
        isDead = true;
        Instantiate(deathExplosion, transform.position, transform.rotation);
        playerController.deathUI.enabled = true;
        playerController.deathUI.GetComponentInChildren<FadeGroup>().fade();
        Destroy(gameObject);
    }
    public string getShipName()
    {
        return shipName;
    }
    public int getShipPrice()
    {
        return shipPrice;
    }
    private void zoomToShipSize()
    {
        Transform camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
        Vector3 pos = camera.transform.position;
        pos.z = -sizeModifier;
        camera.transform.position = pos;
    }
    
}

