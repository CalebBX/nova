﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataController : MonoBehaviour {

    
    public static float money;
    public static float xp;
    public static Item[] inventory = new Item[18];

    public static void resetStaticVars()
    {
        money = 0;
        xp = 0;
        inventory = new Item[18];
    }
    public static void addMoney(float money)
    {
        DataController.money += money;
    }
    public static void removeMoney(float money)
    {
        DataController.money -= money;
    }
    public static void addXp(float xp)
    {
        DataController.xp += xp;
    }
    public static void removeXp(float xp)
    {
        DataController.xp -= xp;
    }
    public static bool addItem(Item item)
    {
        for(int i = 0; i < inventory.Length; i++)
        {
            if(inventory[i] == null)
            {
                inventory[i] = item;
                return true;
            }
        }
        return false;
    }
    public static void removeItem(int slotNum)
    {
        inventory[slotNum] = null;
    }
    public static void setItem(int slotNum, Item item)
    {
        inventory[slotNum] = item;
    }
}
