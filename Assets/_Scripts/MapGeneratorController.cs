﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGeneratorController : MonoBehaviour
{
    private Sprite[] backgrounds;
    private GameObject[] planets;
    private GameObject player;
    private int seed;

    private static float mapX;
    private static float mapY;
    private float paddedMapX;
    private float paddedMapY;

    [Header("Parents")]
    public SpriteRenderer backgroundParent;
    public Transform planetParent;
    public Transform astroidParent;
    public GameObject astroidGroup;
    public Transform enemyParent;
    public GameObject enemyBase;
    public GameObject enemyGroup;

    [Space]

    [Header("Generation Variables")]
    public float edgePadding;
    public int minPlanets;
    public int maxPlanets;
    public int minAstroidGroups = 0;
    public int maxAstroidGroups = 3;
    public int minBases;
    public int maxBases;
    public int minEnemyGroups;
    public int maxEnemyGroups;


    private int layerMask = ~(1 << 11);

    private void Awake()
    {
        backgrounds = Resources.LoadAll<Sprite>("Backgrounds");
        setBackground();
    }
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        seed = System.Environment.TickCount;
        Random.InitState(seed);
        planets = Resources.LoadAll<GameObject>("Planets");
        generateMap(); 
    }
    public void generateMap()
    {
        spawnAstroidGroups();
        spawnPlanets();
        spawnEnemies();
        spawnEnemyBases();
        setRandomPlayerPosition();
    }
    private void setBackground()
    {
        int iBackground =  Random.Range(0, backgrounds.Length);
        backgroundParent.sprite = backgrounds[iBackground];
        setMapSize();
    }
    private void spawnAstroidGroups()
    {
        int numberOfgroups = Random.Range(minAstroidGroups, maxAstroidGroups);
        int spawnAttempt = 0;
        while(spawnAttempt < numberOfgroups)
        {
            Vector3 position = random2DMapPosition();
            Instantiate(astroidGroup, astroidParent.position + position, transform.rotation, astroidParent);
            spawnAttempt++;
        }

    }
    private void spawnPlanets()
    {
        int numberToSpawn = Random.Range(minPlanets, maxPlanets);
        int spawnAttempt = 0;
        while(spawnAttempt < numberToSpawn)
        {
            int iPlanet = Random.Range(0, planets.Length);
            GameObject planetToSpawn = planets[iPlanet];
            Vector3 spawnPosition = random2DMapPosition();
            
            float collisionRadius = planetToSpawn.GetComponent<SpriteRenderer>().bounds.size.x;
            if (Physics2D.OverlapCircle(spawnPosition, collisionRadius, layerMask) == false)
            {
                Quaternion spawnRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
                Instantiate(planetToSpawn, planetParent.position + spawnPosition, spawnRotation, planetParent);
                spawnAttempt++;
            }
        }
    }
    private void spawnEnemies()
    {
        int numberOfgroups = Random.Range(minEnemyGroups, maxEnemyGroups);
        int spawnAttempt = 0;
        while (spawnAttempt < numberOfgroups)
        {
            Vector3 position = random2DMapPosition();
            Instantiate(enemyGroup, enemyParent.position + position, transform.rotation, enemyParent);
            spawnAttempt++;
        }
    }
    private void spawnEnemyBases()
    {
        int numberToSpawn = Random.Range(minBases, maxBases);
        int spawnAttempt = 0;
        while (spawnAttempt < numberToSpawn)
        {
            Vector3 spawnPosition = random2DMapPosition();

            float collisionRadius = enemyBase.GetComponent<SpriteRenderer>().bounds.size.x;
            if (Physics2D.OverlapCircle(spawnPosition, collisionRadius, layerMask) == false)
            {
                Quaternion spawnRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
                Instantiate(enemyBase, enemyParent.position + spawnPosition, spawnRotation, enemyParent);
                Instantiate(enemyGroup, enemyParent.position + spawnPosition, transform.rotation, enemyParent);
                spawnAttempt++;
            }
        }
    }
    private void setMapSize()
    {
        mapX = backgroundParent.bounds.size.x;
        mapY = backgroundParent.bounds.size.y;
    }
    public static Vector3 random2DMapPosition()
    {
        return random2DMapPosition(0);
    }
    public static Vector3 random2DMapPosition(float z)
    {
        float x = Random.Range(-mapX / 2, mapX / 2);
        float y = Random.Range(-mapY / 2, mapY / 2);
        return new Vector3(x, y, 0);
    }
    public void setRandomPlayerPosition()
    {
        bool spawned = false;
        int count = 0;
        while (!spawned)
        {
            count++;

            Vector3 spawnPosition = random2DMapPosition();

            float collisionRadius;
            SpriteRenderer sprite = player.GetComponentInChildren<SpriteRenderer>();
            float xBounds = sprite.bounds.size.x;
            float yBounds = sprite.bounds.size.y;

            if (xBounds >= yBounds)
            {
                collisionRadius = xBounds;
            }
            else
            {
                collisionRadius = yBounds;
            }

            if (Physics2D.OverlapCircle(spawnPosition, collisionRadius) == false)
            {
                player.transform.position = spawnPosition;
                spawned = true;
            }
            if (count > 100000)
            {
                print("LOOP ERROR");
                break;
            }
        }
    }

}
