﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeGroup : MonoBehaviour
{
    public CanvasGroup group;
    public float time;

    public void fade()
    {
        StartCoroutine("FadeIn");
    }

    IEnumerator FadeIn()
    {
        while (group.alpha < 1)
        {
            group.alpha += Time.deltaTime / time;
            yield return null;
        }
    }
}
