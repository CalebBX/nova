﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBaseController : MonoBehaviour {
    public Transform turret;
    public Transform target;
    public Transform shotSpawn;
    public GameObject shot;

    public float fireRate;
    public float turnSpeed;
    private float timer;
    public float deAggroDistance;//must be larger than players aggro
    private EnemyController enemyController;


    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        enemyController = gameObject.GetComponent<EnemyController>();
        timer = 0;
    }


    void Update ()
    {

        if (target != null && enemyController.aggro)
        {
            attack();
        }
  
    }
    private void attack()
    {
        if (Vector3.Distance(transform.position, target.position) > deAggroDistance)
        {
            enemyController.aggro = false;
        }
        Vector3 dir = target.position - turret.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
        turret.rotation = Quaternion.Lerp(turret.rotation, Quaternion.AngleAxis(angle, Vector3.forward), turnSpeed);

        timer += Time.deltaTime;
        if (timer >= fireRate)
        {
            shoot();
        }
    }
    private void shoot()
    {
        timer = 0;
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
    }
}
