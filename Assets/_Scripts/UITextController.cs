﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextController : MonoBehaviour {
    public Text txtMoney;
    public Text txtXp;
    public Text txtHp;
    public PlayerController player;

    private void Update()
    {
        updateText();
    }
    public void updateText()
    {
        txtMoney.text = "Money: " + DataController.money;
        txtXp.text = "XP: " + DataController.xp;
        txtHp.text = "HP: " + player.playerShip.armor;
    }
}
