﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BasicCameraControllerOLD : MonoBehaviour
{
    public Transform player;
    public float m_DampTime = 0.2f;                 // Approximate time for the camera to refocus.
    public GameObject map;
    private Camera m_Camera;                        // Used for referencing the camera.
    private Vector3 m_MoveVelocity;                 // Reference velocity for the smooth damping of the position.
    private Vector3 m_DesiredPosition;              // The position the camera is moving towards.

    private float vertExtent, horzExtent;
    private float mapX;
    private float mapY;
    private float cameraMinX, cameraMaxX, cameraMinY, cameraMaxY;
    private float minX, maxX, minY, maxY;
    private float speed;



    void Awake()
    {
        m_Camera = GetComponentInChildren<Camera>();
        findMapSize();
        findExtent();
    }


    void FixedUpdate()
    {
        Move();
    }


    void Move()
    {

        m_DesiredPosition = player.position;
        m_DesiredPosition = clampTransform(m_DesiredPosition, 0, 0);
        //midPointBoundry();
        //transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
        transform.position = m_DesiredPosition;
    }


    void midPointBoundry()
    {
        Vector3 withinBounds = m_DesiredPosition;
        withinBounds.x = Mathf.Clamp(m_DesiredPosition.x, minX, maxX);
        withinBounds.y = Mathf.Clamp(m_DesiredPosition.y, minY, maxY);
        m_DesiredPosition = withinBounds;
    }
    void findExtent()
    {
        vertExtent = m_Camera.orthographicSize;
        horzExtent = vertExtent * Screen.width / Screen.height;
        vertExtent = (-vertExtent / (speed - 1)) / 2;
        horzExtent = (-horzExtent / (speed - 1)) / 2;
        print("Horizontal " + horzExtent);
        print("Vertical " + vertExtent);
    }
    void findMapSize()
    {
        SpriteRenderer mapSprite = map.GetComponent<SpriteRenderer>();
        Scroller mapScroll = map.GetComponent<Scroller>();
        float x = mapSprite.bounds.size.x / 2;
        float y = mapSprite.bounds.size.y / 2;
        speed = mapScroll.speed;
        mapX = (-x / (speed - 1)) * 2;
        mapY = (-y / (speed - 1)) * 2;
        minX = -mapX / 2;
        maxX = mapX / 2;
        minY = -mapY / 2;
        maxY = mapY / 2;

    }
    public Vector3 clampTransform(Vector3 transPos)
    {
        Vector3 pos = transPos;
        pos.x = Mathf.Clamp(transPos.x, minX, maxX);
        pos.y = Mathf.Clamp(transPos.y, minY, maxY);
        transPos = pos;
        return transPos;
    }
    public Vector3 clampTransform(Vector3 transPos, float vPadding, float hPadding)
    {
        Vector3 pos = transPos;
        float paddedMinX = minX + hPadding;
        float paddedMaxX = maxX - hPadding;
        float paddedMinY = minY + vPadding;
        float paddedMaxY = maxY - vPadding;
        pos.x = Mathf.Clamp(transPos.x, paddedMinX, paddedMaxX);
        pos.y = Mathf.Clamp(transPos.y, paddedMinY, paddedMaxY);
        transPos = pos;
        return transPos;
    }
    public float getMapX()
    {
        return mapX;
    }
    public float getMapY()
    {
        return mapY;
    }

}