﻿
using System.Collections.Generic;
using UnityEngine;
using System;


public static class Items
{
    private static List<Item> itemsList = new List<Item>();
    private static Dictionary<string, Item> itemPresets = new Dictionary<string, Item>();
    public static TextAsset itemFile;

    static Items()
    {
        CreateAllItems();
    }
     

    public static void CreateAllItems()
    {
        
        itemFile = Resources.Load<TextAsset>("TextAssets/Items");
        //string[] lines = System.IO.File.ReadAllLines("Assets/Items.txt");
        char[] archDelim = new char[] { '\r', '\n' };
        itemFile.text.ToString();
        string[] lines = itemFile.text.Split(archDelim, StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < lines.Length;)
        {
            Item item = new Item(lines[i++], lines[i++], float.Parse(lines[i++]), float.Parse(lines[i++]), lines[i++]);
            itemsList.Add(item);
            itemPresets.Add(item.itemName, item);
        }
    }
    public static Item getItem(string name)
    {
        return itemPresets[name];
    }
    public static Item getRandomItem()
    {
        int itemNumber = UnityEngine.Random.Range(0, Items.itemsList.Count);
        return itemsList[itemNumber];
    }
}
