﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{

    public string itemName;
    public string itemType;
    public float damage;
    public float fireRate;
    public string inventorySpriteName;
    public int equip;

    public Item(string itemName, string itemType, float damage, float fireRate, string inventorySpriteName)
    {
        this.itemName = itemName;
        this.itemType = itemType;
        this.damage = damage;
        this.fireRate = fireRate;
        this.inventorySpriteName = inventorySpriteName;
        equip = -1;
        
    }
    public Sprite getSprite()
    {
        return Resources.Load<Sprite>("UISprites/" + this.inventorySpriteName);
    }
    public GameObject getGameObject()
    {
        return Resources.Load<GameObject>("ItemObjects/" + this.itemType);
    }
}

