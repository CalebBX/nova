﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneItem : MonoBehaviour
{
    
    Item reward;
    public string itemName;
    private void Start()
    {
        reward = Items.getRandomItem();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ship")
        {
            DataController.addItem(reward);
            Destroy(gameObject);
        }
    }
}
