﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardpointController : MonoBehaviour
{
    public int rotation;
    public enum HardpointClass{I,II,III}
    public HardpointClass hardpointClass;
    private bool fire = false;



    public bool hasEquip()
    {
        if (transform.childCount > 0)
        {
            return true;
        }
        return false;
    }
    public GameObject getEquip()
    {
        return GetComponentInChildren<Transform>().gameObject;

    }
    public void setFire(bool fire)
    {
        this.fire = fire;
    }
    public bool getFire()
    {
        return fire;
    }
}
