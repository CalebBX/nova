﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGroupSpawnerController : MonoBehaviour {

    const float MIN_DENSITY = 0.1f;
    const float MAX_DENSITY = 1.5f;
    const int FAILCASE = 1000;

    private GameObject[] enemies;
    public int number;
    public float radius;
    private float density;

    [Header("Generation Variables")]
    public float minRadius;
    public float maxRadius;

    private int layerMask = ~(1 << 11);//removes sensors from collision check

    void Start()
    {
        enemies = Resources.LoadAll<GameObject>("Enemies/SingleEnemies");
        //randomize();
        spawnInRadius();
    }
    void spawnInRadius()
    {
        int enemy;
        float xBounds;
        float yBounds;
        float collisionRadius;
        Vector2 position;
        Quaternion rotation;
        int i = 0;
        int fail = 0;
        while (i < number)
        {
            if (fail >= FAILCASE)
            {
                break;
            }
            fail++;
            //enemy = Random.Range(0, enemies.Length);
            enemy = 0;

            xBounds = enemies[enemy].GetComponent<SpriteRenderer>().bounds.size.x;

            yBounds = enemies[enemy].GetComponent<SpriteRenderer>().bounds.size.y;

            if (xBounds > yBounds)
            {
                collisionRadius = xBounds;
            }
            else
            {
                collisionRadius = yBounds;
            }
            
            position = Random.insideUnitCircle * radius;
            position = new Vector2(position.x + transform.position.x, position.y + transform.position.y);
            rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            if (Physics2D.OverlapCircle(position, collisionRadius, layerMask) == false)
            {
                Instantiate(enemies[enemy], position, rotation, transform);
                i++;
                fail = 0;
            }
            if (fail >= FAILCASE)
            {
                print("Number of enemies to great for raidius. Failcase met");
            }

        }
    }
    private void randomize()
    {
        radius = Random.Range(minRadius, maxRadius);
        density = Random.Range(MIN_DENSITY, MAX_DENSITY);
        number = Mathf.RoundToInt(radius * density);
    }
    public float getRadius()
    {
        return radius;
    }
}
