﻿using UnityEngine.UI;
using System.Collections;
using UnityEngine;

public class MessageBoxController : MonoBehaviour {


    public float letterDelay;
    public float lineDelay;
    public Text header;
    public Text message;
    public Button btnAccept;

    public void initialize(string header, string text)
    {
        this.header.text = header;
        StartCoroutine(dialogDelay(text));
        Time.timeScale = 0;
    }
    public void initialize(string header, string[] text)
    {
        this.header.text = header;
        StartCoroutine(dialogDelay(text));
        Time.timeScale = 0;
    }
   
    private IEnumerator dialogDelay(string text)
    {
        string currentText = "";
        for (int i = 0; i < text.Length; i++)
        {
            currentText += text.Substring(i, 1);
            message.text = currentText;
            yield return new WaitForSecondsRealtime(letterDelay);
        }
        btnAccept.interactable = true;
    }
    private IEnumerator dialogDelay(string[] text)
    {
        string currentText = "";
        for (int j = 0; j < text.Length; j++)
        {
            string line = text[j];
            for (int i = 0; i < line.Length; i++)
            {
                currentText += line.Substring(i, 1);
                message.text = currentText;
                yield return new WaitForSecondsRealtime(letterDelay);
            }
            yield return new WaitForSecondsRealtime(lineDelay);
            currentText += " ";
        }
        btnAccept.interactable = true;
    }
    public void close()
    {
        Time.timeScale = 1;
        Destroy(gameObject);
    }


}
