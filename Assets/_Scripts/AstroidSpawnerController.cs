﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidSpawnerController : MonoBehaviour {

    public BasicCameraController cameraController;
    private GameObject[] astroids;
    public int numberOfAstroidsToSpawn;
	// Use this for initialization
	void Start ()
    {
        astroids = Resources.LoadAll<GameObject>("Astroids");
        spawnRandomOnMap(numberOfAstroidsToSpawn);
    
    }
    public void spawnRandomOnMap(int number)
    {
        //float minX = -cameraController.getMapX() / 2;
       // float maxX = cameraController.getMapX() / 2;
        //float minY = -cameraController.getMapY() / 2;
        //float maxY = cameraController.getMapY() / 2;
       // spawnRandom(minX, maxX, minY, maxY, number);
    }
    public void spawnRandom(float minX, float maxX, float minY, float maxY, int number)
    {
        
        int astroid;
        Vector3 position;
        Quaternion rotation;
        int i = 0;
        while(i <= number)
        {
            astroid = Random.Range(0, astroids.Length - 1);
            position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0);
            rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            if (Physics2D.OverlapCircle(position, 1) == false)
            {
                Instantiate(astroids[astroid], position, rotation, transform);
                i++;
            }
        }
    }
}
