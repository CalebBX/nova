﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rewardSpawner : MonoBehaviour {
    public GameObject coin;
    public float modifier;

	public void spawnReward(int coinAmount, Vector2 position)
    {
        
        for (int i = 0; i < coinAmount; i++)
        {
            float x = Random.Range(-modifier, modifier);
            float y = Random.Range(-modifier, modifier);
            Vector2 randMod = new Vector2(x, y);
            Instantiate(coin, position + randMod, Quaternion.Euler(0, 0, Random.rotation.z));
        }
    }
}
