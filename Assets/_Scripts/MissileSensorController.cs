﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSensorController : MonoBehaviour {

    private List<Transform> targets;
    private bool hasTarget = false;
    private Transform target;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("collison");
        if(collision.tag == "Enemy" && !hasTarget)
        {
            print("add targets");
            targets.Add(collision.transform);
        }
    }
    private void Update()
    {
        if(!hasTarget && targets != null)
        {
            target = GetClosestTarget(targets);
            hasTarget = true;
            print("has target");
            gameObject.GetComponentInParent<MissileMover>().setTarget(target);
        }
    }
    Transform GetClosestTarget(List<Transform> targets)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in targets)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }

}
