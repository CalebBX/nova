﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidGroupSpawnerController : MonoBehaviour
{
    const float MIN_DENSITY = 0.1f;
    const float MAX_DENSITY = 1.5f;
    const int FAILCASE = 1000;

    private GameObject[] astroids;
    private int number;
    private float radius;
    private float density;

    private int layerMask = ~(1 << 11);


    [Header("Generation Variables")]
    public float minRadius;
    public float maxRadius;

    void Start()
    {
        astroids = Resources.LoadAll<GameObject>("Astroids");
        randomize();
        spawnInRadius();
    }
    void spawnInRadius()
    {
        int astroid;
        float xBounds;
        float yBounds;
        float collisionRadius;
        Vector2 position;
        Quaternion rotation;
        int i = 0;
        int fail = 0;
        while (i < number)
        {
            if (fail >= FAILCASE)
            {
                break;
            }
            fail++;
            astroid = Random.Range(0, astroids.Length - 1);

            xBounds = astroids[astroid].GetComponent<SpriteRenderer>().bounds.size.x;
            
            yBounds = astroids[astroid].GetComponent<SpriteRenderer>().bounds.size.y;
            
            if (xBounds>yBounds)
            {
                collisionRadius = xBounds;
            }
            else
            {
                collisionRadius = yBounds;
            }

            position = Random.insideUnitCircle * radius;
            position = new Vector2(position.x + transform.position.x, position.y + transform.position.y);
            rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            if (Physics2D.OverlapCircle(position, collisionRadius, layerMask) == false)
            {
                Instantiate(astroids[astroid], position, rotation, transform);
                i++;
                fail = 0;
            }
            if (fail >= FAILCASE)
            {
                print("Number of Astroids to great for raidius. Failcase met");
            }
        }
    }
    private void randomize()
    {
        radius = Random.Range(minRadius, maxRadius);
        density = Random.Range(MIN_DENSITY, MAX_DENSITY);
        number = Mathf.RoundToInt(radius * density);
    }
    public float getRadius()
    {
        return radius;
    }

}