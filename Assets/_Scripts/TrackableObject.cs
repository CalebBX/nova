﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackableObject : MonoBehaviour {

    [HideInInspector]
    public bool isTracked = false;
}
