﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreenController : MonoBehaviour {
    public void loadMap()
    {
        SceneManager.LoadScene("Preload");
        SceneManager.LoadScene("Map");

    }
    public void loadTutorial()
    {
        SceneManager.LoadScene("Preload");
        SceneManager.LoadScene("Tutorial");
    }
}
