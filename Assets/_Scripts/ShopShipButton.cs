﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopShipButton : MonoBehaviour {

    string shipName;
    int shipPrice;
    GameObject ship;
    public GameObject player;
    private PlayerController playerController;
    private FittingController2 fittingController;
    ShipController shipController;

    private void Start()
    {
        fittingController = FindObjectOfType<FittingController2>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<PlayerController>();
    }
    public void InitializeButton(GameObject ship)
    {
        this.ship = ship;
        shipController = ship.GetComponent<ShipController>();
        this.shipName = shipController.getShipName();
        this.shipPrice = shipController.getShipPrice();
        setText();
    }
    void setText()
    {
        transform.Find("txtName").GetComponent<Text>().text = shipName;
        transform.Find("txtPrice").GetComponent<Text>().text = shipPrice.ToString();

    }
    public void changeShip()
    {
        if(DataController.money >= shipPrice)
        {
            DataController.removeMoney(shipPrice);
            foreach (Transform child in player.transform)
            {
                if (child.tag == "Ship")
                {
                    Destroy(child.gameObject);
                }
            }
            GameObject newShip = Instantiate(ship, player.transform.position, player.transform.rotation);
            newShip.transform.SetParent(player.transform, true);

            playerController.shipChange(newShip.GetComponentInChildren<ShipController>());
            fittingController.shipChange(newShip.GetComponent<ShipController>());
        }
      
    }
}
