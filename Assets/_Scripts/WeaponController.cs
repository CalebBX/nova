﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

    public float damage;
    public float fireRate;

    public void setStats(Item item)
    {
        this.damage = item.damage;
        this.fireRate = item.fireRate;
        loadWeapon(item);
    }
    private void loadWeapon(Item item)
    {
        switch(item.itemType)
        {
            case "cannon":
                gameObject.GetComponent<CannonController>().enabled = true;
                break;
            case "missileLauncher":
                gameObject.GetComponent<MissileLauncherController>().enabled = true;
                break;
            case "autoTurret":
                gameObject.GetComponent<turretController>().enabled = true;
                break;
            case "laserTurret":
                gameObject.GetComponent<LaserTurretController>().enabled = true;
                break;
        }

        
        
    }
}
