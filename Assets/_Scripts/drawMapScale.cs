﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class drawMapScale : MonoBehaviour {

    float mapX, mapY;
    float vertExtent, horzExtent;
    float minX, maxX, minY, maxY;
    public float speed;
    public bool update;
    public SpriteRenderer mapSprite;

    private void Start()
    {
        findMapSize();

    }

    void Update () {

        if (update)
        {
            findMapSize();
        }
        //horz
        Debug.DrawLine(new Vector3(minX, minY, 0), new Vector3(maxX, minY, 0));
        Debug.DrawLine(new Vector3(minX, maxY, 0), new Vector3(maxX, maxY, 0));
        //vert
        Debug.DrawLine(new Vector3(minX, minY, 0), new Vector3(minX, maxY, 0));
        Debug.DrawLine(new Vector3(maxX, minY, 0), new Vector3(maxX, maxY, 0));
    }

    void findMapSize()
    {
        
        float x = mapSprite.bounds.size.x / 2;
        float y = mapSprite.bounds.size.y / 2;
        mapX = (-x / (speed - 1)) * 2;
        mapY = (-y / (speed - 1)) * 2;
        vertExtent = Camera.main.orthographicSize;
        horzExtent = vertExtent * Screen.width / Screen.height;
        vertExtent = (-vertExtent / (speed - 1));
        horzExtent = (-horzExtent / (speed - 1));
        minX = (-mapX / 2) + transform.position.x;
        maxX = (mapX / 2) + transform.position.x;
        minY = (-mapY / 2) + transform.position.y;
        maxY = (mapY / 2) + transform.position.y;

    }

}
