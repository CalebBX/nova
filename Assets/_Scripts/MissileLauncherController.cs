﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncherController : MonoBehaviour
{

    public float damage;
    public float fireRate;
    public WeaponController weapon;
    float timer;
    private bool fire = false;
    HardpointController hardpoint;

    public GameObject shot;

    private void Start()
    {
        hardpoint = GetComponentInParent<HardpointController>();
        loadStats();
    }
    private void loadStats()
    {
        damage = weapon.damage;
        fireRate = weapon.fireRate;
    }
    void Update()
    {
        timer += Time.deltaTime;
        fire = hardpoint.getFire();
        if (fire)
        {
            if (timer >= fireRate)
            {
                shoot();
            }
        }
    }
    void shoot()
    {
        timer = 0;
        MissileMover missile = Instantiate(shot, transform.position, transform.rotation).GetComponent<MissileMover>();
        missile.setDamage(damage);
    }
}
