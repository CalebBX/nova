﻿
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class FittingController2 : MonoBehaviour
{
    public Color defaultColor;
    public Color selectedColor;

    public Text txtName;
    public Text txtStat;
    public Transform inventoryGroup;
    public GameObject hardpointSlot;
    public Transform hardpointGroup;
    public Button btnEquip;

    private InventorySlotController[] inventorySlots;
    private HardpointSlotController[] hardpointSlots;

    private int selectedHardpointSlot = -1;
    private int selectedInventorySlot = -1;

    private Sprite defaultSprite;
    private Canvas mainCanvas;
    private ShipController ship;

    private void Start()
    {
        mainCanvas = GetComponent<Canvas>();
        inventorySlots = inventoryGroup.gameObject.GetComponentsInChildren<InventorySlotController>();
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            inventorySlots[i].setSlotNum(i);
        }
        ship = GameObject.FindGameObjectWithTag("Ship").GetComponent<ShipController>();
        defaultSprite = inventorySlots[0].GetComponent<Image>().sprite;

        shipChange(ship);
        startingItems();
        updateAll();
        updateLoadout();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (mainCanvas.enabled)
            {
                mainCanvas.enabled = false;
            }
            else
            {
                mainCanvas.enabled = true;
                updateAll();
            }
        }
    }
    private void updateAll()
    {
        updateItems();
        updateHardpoints();
    }
    private void updateItems()
    {
        for (int i = 0; i < DataController.inventory.Length; i++)
        {
            Button button = inventorySlots[i].GetComponent<Button>();
            Image image = inventorySlots[i].GetComponent<Image>();
            if (DataController.inventory[i] != null)
            {
                inventorySlots[i].item = DataController.inventory[i];
                inventorySlots[i].hasItem = true;
                image.sprite = inventorySlots[i].item.getSprite();
                button.interactable = true;
                if(i == selectedInventorySlot)
                {
                    image.color = selectedColor;
                }
                else
                {
                    image.color = defaultColor;
                }
            }
            else
            {
                inventorySlots[i].hasItem = false;
                image.sprite = defaultSprite;
                image.color = defaultColor;
                button.interactable = false;
            }
        }
        btnEquip.interactable = (selectedHardpointSlot >= 0 && selectedInventorySlot >= 0);
        if(selectedInventorySlot >= 0)
        {
            showItemsStats();
        } 
        else
        {
            clearItemStats();
        }
    }
    private void updateHardpoints()
    {
        for(int i = 0; i < hardpointSlots.Length; i++)
        {
            Image image = hardpointSlots[i].GetComponent<Image>();
            if(hardpointSlots[i].hasItem)
            {
                image.sprite = hardpointSlots[i].item.getSprite();
            }
            if (i == selectedHardpointSlot)
            {
                image.color = selectedColor;
            }
            else
            {
                image.color = defaultColor;
            }
        }
        btnEquip.interactable = (selectedHardpointSlot >= 0 && selectedInventorySlot >= 0);
    }
    public void selectHardpoint(int slotNum)
    {
        if(selectedHardpointSlot == slotNum)
        {
            selectedHardpointSlot = -1;
        }else
        {
            selectedHardpointSlot = slotNum;
        }
        updateAll();
    }
    public void selectItem(int slotNum)
    {
        if(selectedInventorySlot == slotNum)
        {
            selectedInventorySlot = -1;
        }
        else
        {
            selectedInventorySlot = slotNum;
        }
        updateAll();
    }
    private void showItemsStats()
    {
        Item selectedItem = inventorySlots[selectedInventorySlot].item;
        txtName.text = selectedItem.itemName;
        txtStat.text = "Type: " + selectedItem.itemType + "\n" +
                        "Damage: " + selectedItem.damage + "\n" +
                        "Fire Rate: " + selectedItem.fireRate + "\n";
    }
    private void clearItemStats()
    {
        txtName.text = "";
        txtStat.text = "";
    }
    public void equipItem()
    {
        if (hardpointSlots[selectedHardpointSlot].hasItem)
        {
            Item temp = hardpointSlots[selectedHardpointSlot].item;
            hardpointSlots[selectedHardpointSlot].item = DataController.inventory[selectedInventorySlot];
            DataController.setItem(selectedInventorySlot, temp);
        }
        else
        {
            hardpointSlots[selectedHardpointSlot].item = inventorySlots[selectedInventorySlot].item;
            hardpointSlots[selectedHardpointSlot].hasItem = true;
            DataController.removeItem(selectedInventorySlot);
        }
        selectedHardpointSlot = -1;
        selectedInventorySlot = -1;
        updateItems();
        updateHardpoints();
        updateLoadout();
    }
    public void updateLoadout()
    {
        for (int i = 0; i < ship.hardpoints.Length; i++)
        {
            foreach (Transform child in ship.hardpoints[i].transform)
            {
                Destroy(child.gameObject);
            }
        }
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            Transform hardpointTransform = ship.hardpoints[i].transform;
            if (hardpointSlots[i].hasItem)
            {
                Item currentItem = hardpointSlots[i].item;
                WeaponController weapon;
                weapon = Instantiate(currentItem.getGameObject(), hardpointTransform.position, hardpointTransform.rotation, hardpointTransform).GetComponent<WeaponController>();
                weapon.setStats(currentItem);
            }
        }
    }
    public void shipChange(ShipController newShip)
    {
        if (hardpointSlots != null)
        {
            for (int i = 0; i < hardpointSlots.Length; i++)
            {
                if (hardpointSlots[i].hasItem)
                {
                    DataController.addItem(hardpointSlots[i].item);
                }
                Destroy(hardpointSlots[i].gameObject);
            }
        }

        ship = newShip;
        hardpointSlots = new HardpointSlotController[ship.hardpoints.Length];
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            hardpointSlots[i] = Instantiate(hardpointSlot, hardpointGroup).GetComponent<HardpointSlotController>();
            hardpointSlots[i].setSlotNum(i);
        }

    }
    private void startingItems()
    {
        DataController.addItem(Items.getRandomItem());
        DataController.addItem(Items.getRandomItem());
        DataController.addItem(Items.getRandomItem());
        DataController.addItem(Items.getRandomItem());
    }
}
