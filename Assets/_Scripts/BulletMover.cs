﻿using UnityEngine;
using System.Collections;

public class BulletMover : MonoBehaviour {

    public float lifetime;
    private Rigidbody2D rb;
    public float speed;
    public GameObject explosion;
    public SpriteRenderer sprite;
    public string shipRole;
    private float damage;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        rb.AddForce(transform.up * speed);
    }
    public void setDamage(float damage)
    {
        this.damage = damage;
    }
    public float getDamage()
    {
        return damage;
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag != shipRole)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }



    void Awake()
    {
        Destroy(gameObject, lifetime);
    }
}
