﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTurretController : MonoBehaviour {


    public float range;
    public LineRenderer laser;
    float effectsDisplayTime = 0.1f;
    public float fireRate;
    public Transform shotSpawn;
    public PlayerController playerScript;
    private List<Transform> targets;
    public GameObject hitExplosion;
    public float damage;
    float timer;
    public int rayMask;
    public WeaponController weapon;
    private HardpointController hardpoint;
    private bool fire;

    // Use this for initialization
    void Start ()
    {
        hardpoint = GetComponentInParent<HardpointController>();
        playerScript = gameObject.GetComponentInParent<PlayerController>();
        laser.sortingLayerName = "Turrets";
        rayMask = LayerMask.GetMask("Enemy");
        loadStats();
    }
    private void loadStats()
    {
        damage = weapon.damage;
        fireRate = weapon.fireRate;
    }
    void Update ()
    {
        fire = hardpoint.getFire();
        timer += Time.deltaTime;
        targets = playerScript.targets;

        if (fire && timer >=fireRate)
        {
            if (targets.Count > 0)
            {
                Shoot();
            }
        }

        if (timer >= fireRate * effectsDisplayTime)
        {
            laser.enabled = false;
        }

    }
    void Shoot()
    {
        
        timer = 0f;
        //Vector3 dir = (transform.position - GetClosestTarget(targets).position).normalized;
        Vector3 dir = (GetClosestTarget(targets).position - transform.position);
        Debug.DrawRay(transform.position, dir);
        if (Physics2D.Raycast(transform.position, dir, range, rayMask))
            {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, range, rayMask);
                EnemyController enemyController = hit.collider.GetComponent<EnemyController>();
                laser.enabled = true;
                laser.SetPosition(0, transform.position);
                if (enemyController != null)
                {
                    laser.SetPosition(1, hit.point);
                    enemyController.takeDamage(damage);
                    Instantiate(hitExplosion, hit.transform.position, hit.transform.rotation);
                }
                else
                {
                    print(hit.point);

                    laser.SetPosition(1, transform.position + dir * range);
                }
        }
    }
        
    Transform GetClosestTarget(List<Transform> targets)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in targets)
        {
            if(potentialTarget != null)
            {
                Vector3 directionToTarget = potentialTarget.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestTarget = potentialTarget;
                }
            }            
        }
        return bestTarget;
    }
}
