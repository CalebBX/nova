﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackerController : MonoBehaviour {

    public SpriteRenderer spriteRenderer;
    public float maxDistance;
    private Transform target;
    private bool hasTarget = false;
    public float closeDistance;
    public Color closeColor;
    public Color farColor;


    public void Initialize(Transform target)
    {
        this.target = target;
        hasTarget = true;
    }
    void Update()
    {
        if (hasTarget && target != null)
        {
            Vector3 v3Screen = Camera.main.WorldToViewportPoint(target.position);
            if (v3Screen.x > -0.01f && v3Screen.x < 1.01f && v3Screen.y > -0.01f && v3Screen.y < 1.01f)
            {
                spriteRenderer.enabled = false;
            }
            else
            {
                
                spriteRenderer.enabled = true;
                v3Screen.x = Mathf.Clamp(v3Screen.x, 0.01f, 0.99f);
                v3Screen.y = Mathf.Clamp(v3Screen.y, 0.01f, 0.99f);
                transform.position = Camera.main.ViewportToWorldPoint(v3Screen);
                if (Vector2.Distance(transform.position, target.position) < closeDistance)
                {
                    spriteRenderer.color = closeColor;
                }
                else
                {
                    spriteRenderer.color = farColor;
                }
                if (Vector2.Distance(transform.position, target.position) > maxDistance)
                {
                    target.gameObject.GetComponent<TrackableObject>().isTracked = false;
                    Destroy(gameObject);
                }
            }
        }
        else if(hasTarget && target == null)
        {
            Destroy(gameObject);
        }

    }
}
