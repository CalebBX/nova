﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public List<Transform> targets;
    public ShipController playerShip;
    public Transform curTarget;
    public Canvas deathUI;
    public float enemyDistanceToStop;

    private void Awake()
    {
        SceneManager.sceneLoaded += this.OnLoadCallback;
    }
    public void Start()
    {
        playerShip = gameObject.GetComponentInChildren<ShipController>();
        enemyDistanceToStop = gameObject.GetComponentInChildren<ShipController>().sizeModifier;
    }
    public void shipChange(ShipController newShip)
    {
        playerShip = newShip;
        enemyDistanceToStop = newShip.sizeModifier;
    }
    Transform GetClosestEnemy(List<Transform> targets)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in targets)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }
   
    void OnLoadCallback(Scene scene, LoadSceneMode sceneMode)
    {
       
    }
}

