﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFighterAI : MonoBehaviour {

    public HardpointController[] hardPoints;
    public ParticleSystem[] thrusters;
    public float range;
    public float turnspeed;
    public float stopDistance;
    public float speed;
    private int playerLayerMask = (1 << 9);
    private Rigidbody2D rb;
    private Transform target;
    private EnemyController enemyController;

    private void Start()
    {
        hardPoints = gameObject.GetComponentsInChildren<HardpointController>();
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        enemyController = gameObject.GetComponent<EnemyController>();
    }
    private void Update()
    {
        if (enemyController.aggro)
        {
            stopDistance = enemyController.playerController.enemyDistanceToStop;
            attack();
        }
        
    }
    private void attack()
    {
        //Set pos and rot
        Vector3 vectorToTarget = target.position - transform.position;
        float angle = (Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg) - 90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * turnspeed);

        //Check distance to player
        if (Vector3.Distance(target.transform.position, rb.transform.position) > stopDistance)
        {
            rb.AddRelativeForce(new Vector3(0, 1, 0) * speed * Time.deltaTime);
            startThrusters();
        }
        else
        {
            stopThrusters();
        }
        foreach (HardpointController hardPoint in hardPoints)
        {
            if (CheckInAttackRange(hardPoint.transform))
            {
               hardPoint.setFire(true);
            }
            else
            {
                hardPoint.setFire(false);
            }
        }
        
    }
    bool CheckInAttackRange(Transform firePoint)
    {
        RaycastHit2D hit = Physics2D.Raycast(firePoint.position, firePoint.up, range, playerLayerMask);
        if (hit.collider != null && hit.collider.tag == "Ship")
        {
            return true;
        }
        return false;
    }
    private void startThrusters()
    {
        if (thrusters[0].isStopped)
        {
            foreach(ParticleSystem thruster in thrusters)
            {
                thruster.Play();
            }
        }
    }
    private void stopThrusters()
    {
        if (thrusters[0].isPlaying)
        {
           foreach(ParticleSystem thruster in thrusters)
            {
                thruster.Stop();
            }
        }
    }
}

