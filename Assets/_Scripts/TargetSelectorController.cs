﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSelectorController : MonoBehaviour {

    public DialogController dialog;
	// Use this for initialization
	void Start ()
    {
		dialog = GameObject.FindGameObjectWithTag("Dialog").GetComponent<DialogController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint((Input.GetTouch(0).position)), Vector2.zero);
            if (hit.collider != null)
            {
                print("Touched" + hit.transform.gameObject.name);
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint((Input.mousePosition)), Vector2.zero);
            if (hit.collider != null)
            {
                print("Touched" + hit.transform.gameObject.name);
            }
        }
        
            
        
    }
}
