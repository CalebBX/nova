﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class GateController : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ship")
        {
            SceneManager.LoadScene("Map");
        }
    }
}
