﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyAI : MonoBehaviour
{
    public enum Behavior {Fighter, TurretCruiser, Bomber}
    public Behavior EnemyBehavior;
    float enemyHealth = 10;
    
    bool dead;
    Rigidbody2D rb;
    
    Transform target;
    public float speed;
    public float turnspeed;
    public float stopDistance;
    public ParticleSystem thruster;
    public GameObject playerShot;
    GameObject player;
    PlayerController playerScript;
    public GameObject deathExplosion;
    GameObject rewardSpawnPoint;
    rewardSpawner rewSpawner;
    public int coinReward = 10;
    public int xpReward = 3;

    //Attacking
    public float range;
    public Transform shotSpawn;
    private float timer;
    public float fireRate;
    public float damage;
    public GameObject shot;
    public float aggroMultiplier = 1;
    public bool aggro = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();        
        player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
        playerScript = player.GetComponent<PlayerController>();
        rewardSpawnPoint = GameObject.Find("RewardSpawnPoint");
        rewSpawner = (rewardSpawner)rewardSpawnPoint.GetComponent(typeof(rewardSpawner));
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (aggro)
        {
            attack();
        }
        if (enemyHealth <= 0 && dead == false)
        {
            die();
        }


    }
    private void attack()
    {
        if (EnemyBehavior == Behavior.Fighter)
        {
            fighterAttack();
        }
        

    }
    private void fighterAttack()
    {
        Vector3 vectorToTarget = target.position - transform.position;
        float angle = (Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg) - 90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * turnspeed);
        //Check distance to player
        if (Vector3.Distance(target.transform.position, rb.transform.position) > stopDistance)
        {

            rb.AddRelativeForce(new Vector3(0, 1, 0) * speed * Time.deltaTime);
            if (thruster.isStopped)
            {
                thruster.Play();
            }
        }
        else
        {
            if (thruster.isPlaying)
            {
                thruster.Stop();
            }
        }
        if (CheckInAttackRange(shotSpawn) && timer >= fireRate)
        {
            shoot();
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player_Shot")
        {
           takeDamage(coll.GetComponent<BulletMover>().getDamage());
        }
        if (coll.gameObject.tag == "TargetSensor")
        {
            playerScript.targets.Add(transform);
        }
    }
    private void die()
    {
        dead = true;
        Instantiate(deathExplosion, transform.position, transform.rotation);
        rewSpawner.spawnReward(10, transform.position);
        playerScript.targets.Remove(transform);
        DataController.addXp(xpReward);
        Destroy(gameObject);
    }
    public void takeDamage(float damage)
    {
        enemyHealth -= damage;
    }
    bool CheckInAttackRange(Transform firePoint)
    {
        RaycastHit2D hit = Physics2D.Raycast(firePoint.position, firePoint.up, range, 9);
        if (hit.collider != null && hit.collider.tag == "Ship")
        {
            return true;
        }
        return false;
        

    }
    private void shoot()
    {
        timer = 0;
        BulletMover bullet = Instantiate(shot, shotSpawn.position, shotSpawn.rotation).GetComponent<BulletMover>();
        bullet.setDamage(damage);
    }
}

