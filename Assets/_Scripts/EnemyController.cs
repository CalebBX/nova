﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    [Header("Parent Objects")]
    public GameObject deathExplosion;

    [Space]

    public int coinReward;
    public float xpReward;

    public float health;
    private bool dead;
    public bool aggro;
    public PlayerController playerController;
    private rewardSpawner rewSpawner;


    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        rewSpawner = GameObject.FindGameObjectWithTag("RewardSpawner").GetComponent<rewardSpawner>();
    }
    private void Update()
    {
        if (health <= 0 && !dead)
        {
            die();
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player_Shot")
        {
            takeDamage(coll.GetComponent<BulletMover>().getDamage());
        }
        if (coll.gameObject.tag == "TargetSensor")
        {
            playerController.targets.Add(transform);
        }
    }
    private void die()
    {
        dead = true;
        Instantiate(deathExplosion, transform.position, transform.rotation);
        rewSpawner.spawnReward(coinReward, transform.position);
        playerController.targets.Remove(transform);
        DataController.addXp(xpReward);
        Destroy(gameObject);
    }
    public void takeDamage(float damage)
    {
        health -= damage;
    }
}
