﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HardpointSlotController : MonoBehaviour
{
    private int slotNum;
    public Item item;
    public bool hasItem = false;
    public FittingController2 fittingController;
    private void Start()
    {
        fittingController = FindObjectOfType<FittingController2>();
    }
    public void selectSlot()
    {
        fittingController.selectHardpoint(slotNum);
    }
    public void setSlotNum(int slotNum)
    {
        this.slotNum = slotNum;
    }
    public int getSlotNum()
    {
        return slotNum;
    }
}
