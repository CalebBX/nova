﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileMover : MonoBehaviour {

    private List<Transform> targets;
    private Transform target;
    private float damage;
    public float speed;
    public float turnSpeed;
    private Rigidbody2D rb;
    private bool hasTarget;
    public GameObject explosion;
    public float lifetime;

	void Start ()
    {
        hasTarget = false;
        targets = new List<Transform>();
        rb = GetComponent<Rigidbody2D>();
        rb.AddRelativeForce(new Vector3(0, 1, 0) * speed * Time.deltaTime);
    }
    private void Update()
    {
        if (!hasTarget && targets.Count != 0)
        {
            target = GetClosestTarget(targets);
            hasTarget = true;
        }
        if (hasTarget)
        {
            if (target != null)
            {
                chaseTarget();
            }
            else
            {
                rb.AddRelativeForce(new Vector3(0, 1, 0) * speed * Time.deltaTime);
            }
        }
        else
        {
            rb.AddRelativeForce(new Vector3(0, 1, 0) * speed * Time.deltaTime);
        }

        Destroy(gameObject, lifetime);

    }
    public void setTarget(Transform target)
    {
    }
    private void chaseTarget()
    {
        Vector3 vectorToTarget = target.position - transform.position;
        float angle = (Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg) - 90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * turnSpeed);
        rb.AddRelativeForce(new Vector3(0, 1, 0) * speed * Time.deltaTime);
    }
    
    public void setDamage(float damage)
    {
        this.damage = damage;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyController>().takeDamage(damage);
        }
        Destroy(gameObject);
    }
    private void OnDestroy()
    {
        Instantiate(explosion, transform.position, transform.rotation);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if(targets.Contains(collision.transform) == false)
            {
                targets.Add(collision.transform);
            }
        }
    }   
    Transform GetClosestTarget(List<Transform> targets)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in targets)
        {
            if (potentialTarget != null)
            {
                Vector3 directionToTarget = potentialTarget.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestTarget = potentialTarget;
                }
            }
        }

        return bestTarget;
    }
}
