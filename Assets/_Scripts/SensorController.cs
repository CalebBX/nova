﻿//By Joseph Giordano.
using UnityEngine;
using System.Collections.Generic;

//Attach this class to the GameObject you want the arrow to be pointing at.
public class SensorController : MonoBehaviour
{
    public GameObject tracker;
    public Transform player;

    private void Update()
    {
        transform.position = player.position;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if(collision.GetComponent<TrackableObject>().isTracked == false)
            {
                TrackerController spawnedTracker = Instantiate(tracker).GetComponent<TrackerController>();
                spawnedTracker.Initialize(collision.transform);
            }
        }
    }


}