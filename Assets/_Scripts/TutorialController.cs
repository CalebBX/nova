﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour {

    public GameObject messageBoxPrefab;
    private MessageBoxController messageBox;

    private bool inventoryTut = false;
    void Start ()
    {
        string[] message;
        MessageBoxController messageBox = Instantiate(messageBoxPrefab).GetComponent<MessageBoxController>();
        message = new string[]
            {
                "Welcome to nova Recruit.",
                "I trust your hypersleep was peaceful.",
                "Your mission to explore the cosmos will commence shortly, but first we need to go through some calibration procedures with your ship."
            };
        messageBox.initialize("Welcome", message);

    }
    private void Update()
    {
        if(!inventoryTut)
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                inventoryTut = true;
                StartCoroutine(inventoryTutorial());
            }
        }
    }
    private IEnumerator inventoryTutorial()
    {
        yield return new WaitForSeconds(1);
        messageBox = Instantiate(messageBoxPrefab).GetComponent<MessageBoxController>();
        string[] message = new string[]
        {
                    "This is your inventory and fitting screen.",
                    "Here you can customize your loadout and check item stats.",
                    "To equip an item select a hardpoint by clicking a slot, then select the item slot you would like to equip.",
                    "After both slots are selected the equip button will enable.",
                    "Simply click equip to then change your loadout."
        };
        messageBox.initialize("Inventory", message);
    }



}
