﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinController : MonoBehaviour {

    public GameObject player;
    public float magnetDistance;
    
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        magnetDistance = player.GetComponentInChildren<ShipController>().sizeModifier/2;    
    }
	void Update ()
    {
		if (Vector2.Distance(player.transform.position, transform.position) < magnetDistance)
        {
            
            transform.position= Vector2.MoveTowards(transform.position, player.transform.position, 0.1f);
        }
	}
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Ship")
        {
            DataController.addMoney(1);
            Destroy(gameObject);
        }
    }
}
