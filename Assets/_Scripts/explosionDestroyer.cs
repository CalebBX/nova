﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosionDestroyer : MonoBehaviour {

    // Use this for initialization
    void Awake()
    {
        ParticleSystem exp = GetComponent<ParticleSystem>();
        Destroy(gameObject, exp.main.duration);
    }
}
