﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BasicCameraController : MonoBehaviour
{
    private Transform player;
    private float z;
    private void Awake()
    {
        z = transform.position.z;
    }
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void FixedUpdate()
    {
        Move();
    }
    void Move()
    {
        transform.position = player.position; 
    }    
}