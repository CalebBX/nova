using System;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Restarter : MonoBehaviour
{
    public void resetGame(GameObject pObjects)
    {
        SceneManager.UnloadSceneAsync("Map");
        DataController.resetStaticVars();
        Destroy(pObjects);
        SceneManager.LoadScene("Start");
    }
    public static void restart()
    {
        SceneManager.UnloadSceneAsync("Map");
        DataController.resetStaticVars();
        GameObject persistantObjects = GameObject.FindGameObjectWithTag("PersistantObjects").GetComponent<GameObject>();
        Destroy(persistantObjects);
        SceneManager.LoadScene("Start");
    }
}

