﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour {

    private Transform cameraRig;
    public float speed;
    private Vector3 startingPos;
    private void Start()
    {
        cameraRig = GameObject.FindGameObjectWithTag("Camera_Rig").GetComponent<Transform>();
        startingPos = transform.position;
    }
    
	void Update ()
    {
        transform.position = startingPos + (cameraRig.position * speed);
    }
    public float getSpeed()
    {
        return speed;
    }
}
