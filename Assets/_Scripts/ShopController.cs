﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour {

    
    public Canvas mainCanvas;
    public GameObject shipPanel;
    public GameObject shipButton;
    private GameObject[] playerShips;

    private void Start()
    {
        playerShips = Resources.LoadAll<GameObject>("PlayerShips");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            if (mainCanvas.enabled)
            {
                mainCanvas.enabled = false;
            }
            else
            {
                mainCanvas.enabled = true;
                updateButtons();
            }
        }
    }
    public void close()
    {
        mainCanvas.enabled = false;
    }
    public void updateButtons()
    {
        destroyButtons();
        for (int i = 0; i < playerShips.Length; i++)
        {
            GameObject button = Instantiate(shipButton, shipPanel.transform);
            button.transform.SetParent(shipPanel.transform, false);
            ShopShipButton shopShipButton = button.GetComponent<ShopShipButton>();
            shopShipButton.InitializeButton(playerShips[i]);
        }

    }
    public void destroyButtons()
    {
        foreach (Transform button in shipPanel.transform)
        {
            Destroy(button.gameObject);
        }
    }

    public void pause(bool pause)
    {
        if (pause == true)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }
}
