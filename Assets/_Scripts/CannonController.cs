﻿
using UnityEngine;

public class CannonController : MonoBehaviour {

    private float damage;
    private float fireRate;
    public WeaponController weapon;
    float timer;
    private bool fire = false;
    HardpointController hardpoint;
    
    public GameObject shot;

    private void Start()
    {
        hardpoint = GetComponentInParent<HardpointController>();
        loadStats();
    }
    private void loadStats()
    {
        damage = weapon.damage;
        fireRate = weapon.fireRate;
    }
    void Update ()
    {
        fire = hardpoint.getFire();
        if (fire)
        {
            timer += Time.deltaTime;
            if (timer >= fireRate)
            {
                shoot();
            }
        }
    }
    void shoot()
    {
        timer = 0;
        BulletMover bullet = Instantiate(shot, transform.position, transform.rotation).GetComponent<BulletMover>();
        bullet.setDamage(damage);
    }
}
