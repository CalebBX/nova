﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogController : MonoBehaviour {

    public float delay;

    IEnumerator dialogDelay(Text parent, string text,  bool writeOver)
    {
        if (writeOver)
        {
            parent.text = "";
        }
        for (int i = 0; i < text.Length; i++)
        {
            parent.text += text.Substring(i, 1);
            yield return new WaitForSeconds(delay);
        }
    }
   
    public void writeDialog(Text parent, string text)
    {
        StartCoroutine(dialogDelay( parent, text, false));
    }
}
