﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class MiningLaserController : MonoBehaviour
{


    public float range;
    RaycastHit2D shootHit;
    public LineRenderer laser;
    float effectsDisplayTime = 0.1f;
    public float fireRate;
    public Transform shotSpawn;
    public PlayerController playerScript;
    private float nextFire;
    private List<Transform> targets;
    public GameObject hitExplosion;
    public float damage;
    float timer;
    public int rayMask;

    // Use this for initialization
    void Start()
    {
        playerScript = gameObject.GetComponentInParent<PlayerController>();
        laser.sortingLayerName = "Turrets";
        rayMask = LayerMask.GetMask("Shootable");
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        targets = playerScript.targets;

        if (CrossPlatformInputManager.GetButton("Mine") && timer >= fireRate && Time.timeScale != 0)
        {
            if (targets != null)
            {
                Mine();
            }
        }
        else
        {
            laser.enabled = false;
        }

        

    }
    void Mine()
    {
        timer = 0f;
        Vector3 dir = (GetClosestTarget(targets).position - transform.position);
        if (Physics2D.Raycast(transform.position, dir, range, rayMask))
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, range, rayMask);
            EnemyAI enemyAI = hit.collider.GetComponent<EnemyAI>();
            laser.enabled = true;
            laser.SetPosition(0, transform.position);
            if (enemyAI != null)
            {
                laser.SetPosition(1, hit.point);
                enemyAI.takeDamage(damage);
                Instantiate(hitExplosion, hit.transform.position, hit.transform.rotation);
            }
            else
            {
                print(hit.point);

                laser.SetPosition(1, transform.position + dir * range);
            }
        }
    }

    Transform GetClosestTarget(List<Transform> targets)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in targets)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }
}
