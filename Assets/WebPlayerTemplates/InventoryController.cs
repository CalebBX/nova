﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class InventoryController: MonoBehaviour
{
    public GameObject inventorySlot;
    public Transform inventoryGroup;
    public GameObject hardpointSlot;
    public Transform hardpointGroup;
    public GameObject UI;
    private ShipController ship;
    private InventorySlotController[] inventorySlots;
    private HardpointSlotController[] hardpointSlots;
    private int selectedHardpointSlot;
    private Sprite defaultSprite;
   
    private void Start()
    {
        //inventorySlots = inventoryGroup.gameObject.GetComponentsInChildren<InventorySlotController>();
        for (int i =0; i < inventorySlots.Length; i++)
        {
            inventorySlots[i].setSlotNum(i);
        }
        hardpointSlots = hardpointGroup.gameObject.GetComponentsInChildren<HardpointSlotController>();
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            hardpointSlots[i].setSlotNum(i);
        }
        ship = GameObject.FindGameObjectWithTag("Ship").GetComponent<ShipController>();
        defaultSprite = inventorySlots[0].GetComponent<Image>().sprite;
        shipChange(ship);
        startingItems();
        updateItems();
        updateLoadout();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            if(UI.activeSelf)
            {
                UI.SetActive(false);
            }
            else
            {
                showUI();
            }
        }
    }
    private void showUI()
    {
        UI.SetActive(true);
        updateItems();
    }
    public bool addItem(Item item)
    {
        for(int i = 0; i < inventorySlots.Length; i++)
        {
            if(!inventorySlots[i].hasItem)
            {
                inventorySlots[i].item = item;
                inventorySlots[i].hasItem = true;
                updateItems();
                return true;
            }
        }
        return false;
    }
   
    private void spawnHardpointSlots()
    {
        hardpointSlots = new HardpointSlotController[ship.numOfHardpoints];
        for(int i = 0; i < hardpointSlots.Length; i++)
        {
            hardpointSlots[i] = Instantiate(hardpointSlot, hardpointGroup).GetComponent<HardpointSlotController>();
            hardpointSlots[i].setSlotNum(i);
        }
    }
    private void updateItems()
    {
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            if(inventorySlots[i].hasItem)
            {
                inventorySlots[i].gameObject.GetComponent<Image>().sprite = inventorySlots[i].item.getSprite();
                //inventorySlots[i].gameObject.GetComponent<Button>().interactable = true;
                inventorySlots[i].gameObject.GetComponent<Button>().interactable = false;//temporary until allow hardpoint toggle off and inventory view
            }
            else
            {
                inventorySlots[i].gameObject.GetComponent<Image>().sprite = defaultSprite;
                inventorySlots[i].gameObject.GetComponent<Button>().interactable = false;
            }

        }
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            if (hardpointSlots[i].hasItem)
            {
                hardpointSlots[i].gameObject.GetComponent<Image>().sprite = hardpointSlots[i].item.getSprite();
            }
        }
    }
   
    public void selectHardpoint(int slotNum)
    {
        selectedHardpointSlot = slotNum;
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            hardpointSlots[i].GetComponent<Button>().interactable = true;
        }
        hardpointSlots[slotNum].GetComponent<Button>().interactable = false;
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            if(inventorySlots[i].hasItem)
            {
                inventorySlots[i].GetComponent<Button>().interactable = true;
            }
        }
    }
    public void selectItem(int slotNum)
    {
        if(hardpointSlots[selectedHardpointSlot].hasItem)
        {
            Item temp = hardpointSlots[selectedHardpointSlot].item;
            hardpointSlots[selectedHardpointSlot].item = inventorySlots[slotNum].item;
            inventorySlots[slotNum].item = temp;
        }
        else
        {
            hardpointSlots[selectedHardpointSlot].item = inventorySlots[slotNum].item;
            hardpointSlots[selectedHardpointSlot].hasItem = true;
            inventorySlots[slotNum].hasItem = false;
        }
        hardpointSlots[selectedHardpointSlot].GetComponent<Button>().interactable = true;
        updateItems();
        updateLoadout();
    }
    public void updateLoadout()
    {
        for (int i = 0; i < ship.hardpoints.Length; i++)
        {
            foreach (Transform child in ship.hardpoints[i].transform)
            {
                Destroy(child.gameObject);
            }
        }
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            Transform hardpointTransform = ship.hardpoints[i].transform;
            if(hardpointSlots[i].hasItem)
            {
                Instantiate(hardpointSlots[i].item.getGameObject(), hardpointTransform.position, hardpointTransform.rotation, hardpointTransform);
            }
        }
    }
    public void startingItems()
    {
        Items allItems = new Items();
        addItem(allItems.getItem("Cannon I"));
        addItem(allItems.getItem("Missile Launcher I"));
    }
    public void shipChange(ShipController newShip)
    {
        //Adds current mods to inventory
        if(hardpointSlots != null)
        {
            for(int i = 0; i <hardpointSlots.Length;i++)
            {
                if(hardpointSlots[i].hasItem)
                {
                    addItem(hardpointSlots[i].item);
                }
                Destroy(hardpointSlots[i].gameObject);
            }
        }
        
        ship = newShip;
        hardpointSlots = new HardpointSlotController[ship.hardpoints.Length];
        for (int i = 0; i < hardpointSlots.Length; i++)
        {
            hardpointSlots[i] = Instantiate(hardpointSlot, hardpointGroup).GetComponent<HardpointSlotController>();
            hardpointSlots[i].setSlotNum(i);
        }

    }
}
